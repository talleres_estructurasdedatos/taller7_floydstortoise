class ListNode:
    def __init__(self, value):
        self.value = value
        self.next = None

# Punto 2 - Detección de Ciclo en una Lista Enlazada
def has_cycle(head):
    if not head:
        return False

    tortoise = head
    hare = head

    while hare and hare.next:
        tortoise = tortoise.next  # O(1)
        hare = hare.next.next  # O(1)

        if tortoise == hare:
            return True

    return False

# Creación de nodos y asignación de relaciones
node1 = ListNode(1)
node2 = ListNode(2)
node3 = ListNode(3)
node4 = ListNode(4)

node1.next = node2
node2.next = node3
node3.next = node4
node4.next = node2  # Creación de un ciclo

if has_cycle(node1):
    print("La lista enlazada con ciclo tiene un ciclo.")  # O(1)
else:
    print("La lista enlazada con ciclo no tiene un ciclo.")  # O(1)

nodeA = ListNode('A')
nodeB = ListNode('B')
nodeC = ListNode('C')

nodeA.next = nodeB
nodeB.next = nodeC

if has_cycle(nodeA):
    print("La lista enlazada sin ciclo tiene un ciclo.")  # O(1)
else:
    print("La lista enlazada sin ciclo no tiene un ciclo.")  # O(1)

# Punto 3 - Encontrar un Duplicado en una Lista de Números
def encontrar_duplicado(numeros):
    tortuga = numeros[0]  # O(1)
    conejo = numeros[0]  # O(1)
    while True:
        tortuga = numeros[tortuga]  # O(1)
        conejo = numeros[numeros[conejo]]  # O(1)
        if tortuga == conejo:
            break
    tortuga = numeros[0]  # O(1)
    while tortuga != conejo:
        tortuga = numeros[tortuga]  # O(1)
        conejo = numeros[conejo]  # O(1)
    return tortuga

numeros = [3, 1, 3, 4, 2]
duplicado = encontrar_duplicado(numeros)
print(f'El número duplicado en la lista es: {duplicado}')  # O(n), donde n es la longitud de la lista de números



